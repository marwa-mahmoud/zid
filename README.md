# zid
This is a landing page that represents ZID APP Market.
ZID APP Market consists of 2 parts :

Zid developers helps you to Enjoy building application to be used by 5000+ merchants and choose any developments stack you prefer, Zid is the way to get passive income and grow your e-commerce knowledge.

Zid dashboard for Merchants.

# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run all tests
npm test
